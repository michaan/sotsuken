class AddUserBs < ActiveRecord::Migration
  def change
    add_column :user_bs, :gps_ido, :float
    add_column :user_bs, :gps_kedo, :float
    add_column :user_bs, :onetimea, :string
    add_column :user_bs, :twitterdate, :datetime
  end
end
