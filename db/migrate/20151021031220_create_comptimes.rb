class CreateComptimes < ActiveRecord::Migration
  def change
    create_table :comptimes do |t|
      t.datetime :first_comp
      t.datetime :second_comp

      t.timestamps null: false
    end
  end
end
