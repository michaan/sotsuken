class AddUserAs < ActiveRecord::Migration
  def change
    add_column :user_as, :gps_ido, :float
    add_column :user_as, :gps_kedo, :float
    add_column :user_as, :onetimeb, :string
    add_column :user_as, :twitterdate, :datetime
  end
end
